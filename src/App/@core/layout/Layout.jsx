import {
    Route
} from "react-router-dom";
import FastViewAttachments from "../../pages/fast-view-attachments/FastViewAttachments";
import Banner from "../../pages/home/components/Banner/Banner";
import Home from "../../pages/home/Home";
import Header from "./components/Header/Header";
import Sidebar from "./components/Sidebar/Sidebar";
import LayoutStyled from "./Layout.styles";


const Layout = () => {
    return (
        <LayoutStyled className="layout">
            <Header />
            <main className="main">
                <Route exact path="/">
                    <Home />
                </Route>
            </main>
            <Sidebar />
        </LayoutStyled>
    )
}

export default Layout;