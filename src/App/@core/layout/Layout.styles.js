import styled from "styled-components";



const LayoutStyled = styled.div`
    //desktop
    display:grid;
    grid-template-columns: 104px auto 400px;
    font-size: 0.8rem;
    font-family: Rubik;
    font-weight: normal;
    .main{
        padding:1.25rem 3.75rem;
        grid-column-start : 2;
    }
    .banner{
        margin-bottom: 1rem;
    }
`;

export default LayoutStyled;