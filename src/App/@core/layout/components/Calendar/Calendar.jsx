import CalendarStyled from "./Calendar.styles";

const Calendar = () => {
    return (
        <CalendarStyled className="calendar">
            <div className="header flex items-center justify-between">
                <h2 className="title">ישיבות קרובות</h2>
                <div className="month-control flex items-center">
                    <div className="actions">
                        <button className="go-back-month">
                            <i className="fas fa-chevron-right"></i>
                        </button>
                        <button className="go-next-month">
                            <i className="fas fa-chevron-left"></i>
                        </button>
                    </div>
                    <div className="month-info">
                        <i className="far fa-calendar-check ml-3"></i>
                        <span>יולי 21</span>
                    </div>
                </div>
            </div>
            <div className="content">
                <div className="days-in-week">
                    <span>א</span>
                    <span>ב</span>
                    <span>ג</span>
                    <span>ד</span>
                    <span>ה</span>
                    <span>ו</span>
                    <span>ש</span>
                </div>
                <div className="dates">
                    <div className="date empty-date"></div>
                    <div className="date empty-date"></div>
                    <div className="date empty-date"></div>
                    <div className="date empty-date"></div>
                    <div className="date empty-date"></div>
                    <div className="date">1</div>
                    <div className="date">2</div>
                    <div className="date highlighted">3</div>
                    <div className="date">4</div>
                    <div className="date bg-red-100">
                        <span>5</span>
                        <div className="committees">
                            <div className="flex justify-center mb-2">
                                <span className="committee-notification ml-0.5 bg-yellow-500"></span>
                                <span className="committee-notification bg-blue-500"></span>
                            </div>
                            <span className="committee-border bg-red-500"></span>
                        </div>
                    </div>
                    <div className="date">6</div>
                    <div className="date bg-blue-100">
                        <span>7</span>
                        <div className="committees">
                            <div className="flex justify-center mb-2">
                                <span className="committee-notification ml-0.5 bg-yellow-500"></span>
                                <span className="committee-notification bg-blue-500"></span>
                            </div>
                            <span className="committee-border bg-red-500"></span>
                        </div>
                    </div>
                    <div className="date">8</div>
                    <div className="date">9</div>
                    <div className="date highlighted">10</div>
                    <div className="date">11</div>
                    <div className="date">12</div>
                    <div className="date">13</div>
                    <div className="date bg-red-100">
                        <span>14</span>
                        <div className="committees">
                            <div className="flex justify-center mb-2">
                                <span className="committee-notification ml-0.5 bg-yellow-500"></span>
                                <span className="committee-notification bg-blue-500"></span>
                            </div>
                            <span className="committee-border bg-red-500"></span>
                        </div>
                    </div>
                    <div className="date">15</div>
                    <div className="date">16</div>
                    <div className="date highlighted">17</div>
                    <div className="date">18</div>
                    <div className="date">19</div>
                    <div className="date">20</div>
                    <div className="date">21</div>
                    <div className="date">22</div>
                    <div className="date">23</div>
                    <div className="date highlighted">24</div>
                    <div className="date">25</div>
                    <div className="date">26</div>
                    <div className="date">27</div>
                    <div className="date">28</div>
                    <div className="date">29</div>
                    <div className="date">30</div>
                    <div className="date highlighted">31</div>
                    <div className="date empty-date"></div>
                    <div className="date empty-date"></div>
                    <div className="date empty-date"></div>
                    <div className="date empty-date"></div>
                    <div className="date empty-date"></div>
                </div>
            </div>
        </CalendarStyled>
    )
}

export default Calendar;