import styled from 'styled-components';

const CalendarStyled = styled.div`

.header{
    margin-bottom:15px;
}

.month-control{
    .actions{
        font-size: 0.75rem;

        .go-back-month{
            margin-left : 1rem;
        }
    }

    .month-info{
        margin-right:1rem;
        padding:4px 14px;
        color:var(--gray-color);
        background-color:var(--very-light-gray-color);
        border-radius:16px;
    }

    
}

.content{
        padding:10px;
        background-color:var(--very-light-gray-color);
        border-radius:16px;

        .days-in-week{
            color:var(--gray-color);
            margin-bottom:15px;
            font-weight:400;
        }
        .days-in-week , .dates{
            display:grid;
            grid-auto-rows: 1fr;
            grid-template-columns:repeat(7,1fr);
            text-align:center;
        }

        .dates{
            grid-row-gap: 5px;
            font-size:1.25rem;

            .highlighted{
                color:var(--primary-color);
            }

            .date{
                position:relative;
                padding-bottom:3px;
                border-radius: 0.375rem;
                &:not(.empty-date){
                cursor: pointer;
                }
                .committees{
                    .committee-border{
                        display:inline-block;
                        height:3px;
                        width:100%;
                        position:absolute;
                        bottom:0;
                        left:0;
                        border-radius:3px;
                    }
                    .committee-notification{
                    display:inline-block;
                    height:5px;
                    width:5px;
                    border-radius:5px;
                    }
                }
                
            }
        }
    }
`;

export default CalendarStyled;