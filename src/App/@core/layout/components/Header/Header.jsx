import HeaderStyled from "./Header.styles";
import { Link } from "react-router-dom";


const Header = () => {
    return (
        <HeaderStyled className="header">
            <nav className="nav">
                <Link to="/" className="logo">
                    BD
                    </Link>
                <ul className="items">
                    <li className="item">
                        <Link to="/" className="link">
                            <i className="far fa-file-alt"></i>
                                פרוטוקולים
                            </Link>
                    </li>
                    <li className="item">
                        <Link to="/" className="link">
                            <i className="fas fa-list"></i>
                                משימות
                            </Link>
                    </li>
                    <li className="item">
                        <Link to="/" className="link">
                            <i className="far fa-folder"></i>
                                מסמכים
                            </Link>
                    </li>
                    <li className="item mt-auto">
                        <Link to="/" className="link">
                            <i className="fas fa-sign-out-alt"></i>
                            יציאה
                        </Link>
                    </li>
                </ul>
            </nav>
        </HeaderStyled >
    )
}

export default Header;