import styled from "styled-components";

const HeaderStyled = styled.header`
    //desktop
    position : fixed;
    width:104px;
    background-color : #33333E;
    height : 100vh;
    padding : 3.125rem 0.9375rem;
    color : white;
    text-align:center;
    display:flex;
    justify-content:center;
    .nav{
        display:flex;
        flex-direction:column;
    }
    .logo{
        display:block;
        font-weight : bold;
        margin-bottom:7.125rem;
        font-size:1.5rem;
    }

    .items{
        flex:1;
        display:flex;
        flex-direction:column;
        .item{
            margin-bottom:3rem;
            i{
                font-size:1.5rem;
                display:block;
            }

            &:last-child{
                margin-bottom : 0;
            }
        }
    }
`;

export default HeaderStyled;