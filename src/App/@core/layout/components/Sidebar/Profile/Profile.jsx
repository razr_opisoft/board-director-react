import ProfileStyled from "./Profile.styles";
import { Link } from "react-router-dom";

const Profile = ()=>{
    return (
        <ProfileStyled className="profile">
            <div className="info">
                <img className="avatar object-cover rounded-full" src="https://via.placeholder.com/60" alt="profile-image" />
                <div>
                    <h3 className="name">תמיר גורן</h3>
                    <Link to="/" className="subtitle">צפייה בפרופיל</Link>
                </div>
            </div>
            
            <ul className="actions">
                <li className="action">
                    <Link to="/">
                        <i className="far fa-envelope notification-alert"></i>
                    </Link>    
                </li>
                <li className="action">
                    <Link to="/">
                        <i className="fas fa-cog"></i>
                    </Link>
                </li>
                <li className="action">
                    <Link to="/">
                        <i className="far fa-life-ring "></i>
                    </Link>
                </li>
                <li className="action">
                    <Link to="/">
                        <i className="fas fa-info-circle"></i>
                    </Link>
                </li>
                
            </ul>
        </ProfileStyled>
    )
}

export default Profile;