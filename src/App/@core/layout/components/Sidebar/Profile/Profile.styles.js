import styled from 'styled-components';

const ProfileStyled = styled.div`
            display:flex;
            align-items : center;
            justify-content: space-between;

          
            .info{
                    display:flex;
                    align-items:center;

                      .avatar{
                            width : 48px;
                            height : 48px;
                            border : 2px solid var(--primary-color);
                            margin-left:.5rem;
            }

                    .name{
                        font-weight : bold;
                        font-size:1.25rem;
                    }
                    }

                    .actions{
                        display:grid;
                        grid-template-columns : repeat(4,1fr);
                        gap: 1.2rem;
                        color : var(--primary-color);

                    .notification-alert::after{
                        content : '';
                        cursor: default;
                        display: block;
                        background-color:#FD6764;
                        width : 6px;
                        height : 6px;
                        border-radius  : 50%;
                        position:relative;
                        top: -25px;
                        right: -7px;
                        }
                    }



`

export default ProfileStyled;