import Calendar from "../Calendar/Calendar";
import Profile from "./Profile/Profile";
import SidebarStyled from "./Sidebar.styles"

const Sidebar = () => {
    return (
        <SidebarStyled className="sidebar">
            <Profile />
            <Calendar />
        </SidebarStyled>
    )
}

export default Sidebar;