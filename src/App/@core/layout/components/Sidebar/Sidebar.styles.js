import styled from 'styled-components';

const SidebarStyled = styled.aside`
        //desktop
        left:0;
        position:fixed;
        height:100vh;
        width:400px;
        background:white;
        padding:2.25rem 2.75rem;

        .profile{
            display:flex;
            align-items : center;
            margin-bottom : 4.375rem;
            .info{
                    display:flex;
                    }   
        }

        
`;

export default SidebarStyled;