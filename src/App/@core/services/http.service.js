import axios from 'axios';
import config from '../../../config';

import Axios from 'axios';

export const http = Axios.create();
http.interceptors.request.use(req => {
    return req;
},(err)=>{
});

http.interceptors.response.use(
    (res) => {
        return res
    }
    , (err) => {
        if (err.response.status === 302)
            window.location = config.api.url + '/' + location.pathname;
    });


export function get(url, config = null) {
    return http.get(url, { withCredentials: true, ...config });
}

export function post(url, data = null, config = null) {
    return http.post(url, data, { withCredentials: true, ...config });
}
