import { Tooltip } from "@material-ui/core";
import { Annotations } from "@pdftron/webviewer";
import React, { useEffect, useState } from "react";
import { AnnotationCustomDataKeys } from "../../constants/constants";
import ToggleSwitch from '../ToggleSwitch/ToggleSwitch';
import SharedWithDialog from "./SharedWithDialog";

interface AnnotationShareProps {
    annotation: Annotations.Annotation,
    usersToShareWith: { id: number, name: string }[],
}


const AnnotationShare = ({ annotation, usersToShareWith }: AnnotationShareProps) => {
    const [checked, setChecked] = useState((typeof (annotation.getCustomData(AnnotationCustomDataKeys.isPublic)) === 'string' ?
        (annotation.getCustomData(AnnotationCustomDataKeys.isPublic) == "true" ? true : false) : annotation.getCustomData(AnnotationCustomDataKeys.isPublic)));
    const [shareWithDialogIsOpen, setShareWithDialogIsOpen] = useState(false);


    const serialize = (share) => {
        if (!share)
            return '';
        return share.map(u => u.id).join(',');
    }

    const deserialize = (share) => {
        if (!share) return [];
        return share.split(',').map(uid => usersToShareWith.find(u => u.id === parseInt(uid))).filter(x => !!x);
    }


    const getSharedWith = ()=>{
        return deserialize(annotation.getCustomData(AnnotationCustomDataKeys.sharedWith));
    }

    const getShareWith = ()=>{
        return usersToShareWith.filter(u => !getSharedWith().find(sw => sw.id === u.id));
    }
    const onChange = (evt) => {
        evt.stopPropagation();
        setChecked(evt.target.checked);
        annotation.setCustomData(AnnotationCustomDataKeys.isPublic, evt.target.checked);
    }

    const openSharewith = () => {
        setShareWithDialogIsOpen(true);
    }


    const onShareWithSave = (data: { sharedWith : [], shareWith : [] }) => {
        const serialized = serialize(data.sharedWith);
        annotation.setCustomData(AnnotationCustomDataKeys.sharedWith, serialized);
    }




    return (
        <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', padding: '.5rem .5rem 0 .5rem' }}>
            <div style={{ display: 'flex', alignItems: 'center' }}>
                <ToggleSwitch
                    id={annotation.Id}
                    optionLabels={['פומבי', 'פרטי']}
                    checked={checked}
                    onChange={onChange}
                />
            </div>
            <Tooltip title={`משותף עם ${getSharedWith().length} אנשים`}>
                <button className="share-button" style={{ visibility: checked ? 'visible' : 'hidden', cursor: 'pointer' }} onClick={openSharewith}>שתף עם</button>
            </Tooltip>

            {shareWithDialogIsOpen ? <SharedWithDialog open onClose={() => setShareWithDialogIsOpen(false)} shareWith={getShareWith()} sharedWith={getSharedWith()} onSave={onShareWithSave} /> : null}

        </div>
    )
}


export default AnnotationShare;

