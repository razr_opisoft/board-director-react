import React, { useEffect, useState } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';

const styles = (theme) => ({
    root: {
        margin: 0,
        padding: theme.spacing(2),
    },
    closeButton: {
        position: 'absolute',
        left: theme.spacing(1),
        top: theme.spacing(1),
        color: theme.palette.grey[500],
    },
});

const DialogTitle = withStyles(styles)((props) => {
    const { children, classes, onClose, ...other } = props;
    return (
        <MuiDialogTitle disableTypography className={classes.root} {...other}>
            <Typography variant="h6">{children}</Typography>
            {onClose ? (
                <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
                    <CloseIcon />
                </IconButton>
            ) : null}
        </MuiDialogTitle>
    );
});

const DialogContent = withStyles((theme) => ({
    root: {
        padding: theme.spacing(2),
    },
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
    root: {
        margin: 0,
        padding: theme.spacing(1),
    },
}))(MuiDialogActions);

const SharedWithDialog = ({ shareWith = [], sharedWith = [], open = false, onClose = () => { }, onSave = (sharedWith) => { } }) => {
    const [isOpen, setIsOpen] = useState(open);
    const [shareWithState, setShareWithState] = useState(shareWith);
    const [sharedWithState, setSharedWithState] = useState(sharedWith);

    useEffect(() => {
        setIsOpen(open);
    }, [open])

    const handleClose = () => {
        !!onClose && onClose();
        setIsOpen(false);
    };

    const handleSave = () => {
        !!onSave && onSave({sharedWith :  sharedWithState , shareWith : shareWithState});
        !!onClose && onClose();
        setIsOpen(false);
    }

    const addToSharedWith = (user) => {
        setSharedWithState([...sharedWithState, user]);
        setShareWithState(shareWithState.filter(u => u !== user));
    }

    const removedFromSharedWith = (user) => {
        setShareWithState([...shareWithState, user]);
        setSharedWithState(sharedWithState.filter(u => u !== user));
    }

    const shareWithAllUsers = () => {
        setShareWithState([]);
        setSharedWithState([...sharedWithState, ...shareWithState]);
    }

    const clearSharedUsers = () => {
        setSharedWithState([]);
        setShareWithState([...sharedWithState, ...shareWithState]);
    }

    return (
        <>
            <Dialog onClose={handleClose} aria-labelledby="customized-dialog-title" open={isOpen} style={{ zIndex: 100001 }}>
                <DialogTitle id="customized-dialog-title" onClose={handleClose}>
                    שתף עם
        </DialogTitle>
                <DialogContent dividers>
                    <div style={{ display: 'flex' }}>
                        <div style={{ marginLeft: '3rem' }}>
                            <h3>בחר משתתפים לשיתוף</h3>
                            <ul style={{ width: '200px', height: '200px', overflow: 'auto', border: '1px solid grey' }}>
                                {shareWithState.map(u => {
                                    return (
                                        <li key={u.id} onClick={() => addToSharedWith(u)} style={{ cursor: 'pointer' }}>{u.name}</li>
                                    )
                                })}
                            </ul>
                            <div style={{ marginTop: '.25em' }}>
                                <button style={{ marginLeft: '1rem' }} onClick={() => shareWithAllUsers()}>הוסף הכל</button>
                            </div>
                        </div>
                        <div>
                            <h3>משותף עם</h3>
                            <ul style={{ width: '200px', height: '200px', overflow: 'auto', border: '1px solid grey' }}>
                                {sharedWithState.map(u => {
                                    return (
                                        <li key={u.id} onClick={() => removedFromSharedWith(u)} style={{ cursor: 'pointer' }}>{u.name}</li>
                                    )
                                })}
                            </ul>
                            <div style={{ marginTop: '.25em' }}>
                                <button onClick={() => clearSharedUsers()}>הסר הכל</button>
                            </div>
                        </div>

                    </div>
                </DialogContent>
                <DialogActions>
                    <Button autoFocus onClick={handleSave} color="primary">
                        שמור
          </Button>
                    <Button onClick={handleClose} color="secondary">
                        בטל
          </Button>
                </DialogActions>
            </Dialog>
        </>
    );
}

export default SharedWithDialog;