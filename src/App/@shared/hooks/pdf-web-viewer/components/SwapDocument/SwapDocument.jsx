import { FormControl, makeStyles, MenuItem, Select } from "@material-ui/core";
import React, { useState } from "react";
import { useDialog } from "../../../dialog/useDialog";


const useStyles = makeStyles((theme) => ({
    formControl: {
        margin: theme.spacing(1),
        width: 200,
        backgroundColor: 'white',
        padding: '5px',
        border: '1px solid gray',
    },
}));

const SwapDocument = ({ relativeDocuments, isDirtyFunc }) => {
    const classes = useStyles();
    const [relativeDocumentSelected, setRelativeDocumentSelected] = useState(relativeDocuments.currentDocument.documentId)
    const { openMessage } = useDialog();

    const handleChange = (event) => {
        if (isDirtyFunc()) {
            openMessage({ message: 'אנא שמור שינויים במסמך קודם', title: 'החלפת מסמך' });
            return;
        }
        setRelativeDocumentSelected(event.target.value);
        relativeDocuments.onChange(event.target.value);
    };
    return (
        <div style={{ position: 'fixed', right: '350px', bottom: '40px', }}>
            <FormControl className={classes.formControl}>
                <Select MenuProps={{ style: { zIndex: 100002 } }} labelId="relative-documents-label" id="relative-documents" value={relativeDocumentSelected} onChange={handleChange}>
                    {relativeDocuments.documents
                        .concat([relativeDocuments.currentDocument])
                        .map((item) => {
                            return <MenuItem value={item.documentId}>{item.name}</MenuItem>
                        })}
                </Select>
            </FormControl>
        </div>
    )
}


export default SwapDocument;