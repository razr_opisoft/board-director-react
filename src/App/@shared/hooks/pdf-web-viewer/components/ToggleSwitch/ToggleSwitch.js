import React, { useState } from "react";

const ToggleSwitch = ({
    id = 'switch',
    name = 'switch',
    checked = false,
    onChange = (evt)=>{},
    optionLabels = ["Yes", "No"],
    small = false,
    disabled = false
}) => {
    const [checkedState, setCheckedState] = useState(checked || false);
    function handleKeyPress(e) {
        if (e.keyCode !== 32) return;
        e.preventDefault();
        onChangeEvt(e);
    }

    const onChangeEvt = (e) => {
        setCheckedState(e.target.checked);
        !!onChange && onChange(e)
    }


    return (
        <div className={"toggle-switch" + (small ? " small-switch" : "")}>
                <input
                    type="checkbox"
                    name={name}
                    className="toggle-switch-checkbox"
                    id={id}
                    checked={checkedState}
                    onChange={onChangeEvt}
                    disabled={disabled}
                />
                {id ? (
                    <label
                        className="toggle-switch-label"
                        tabIndex={disabled ? -1 : 1}
                        onKeyDown={(e) => handleKeyPress(e)}
                        htmlFor={id}
                    >
                        <span
                            className={
                                disabled
                                    ? "toggle-switch-inner toggle-switch-disabled"
                                    : "toggle-switch-inner"
                            }
                            data-yes={optionLabels[0]}
                            data-no={optionLabels[1]}
                            tabIndex={-1}
                        />
                        <span
                            className={
                                disabled
                                    ? "toggle-switch-switch toggle-switch-disabled"
                                    : "toggle-switch-switch"
                            }
                            tabIndex={-1}
                        />
                    </label>
                ) : null}
        </div>
    );
};

export default ToggleSwitch;
