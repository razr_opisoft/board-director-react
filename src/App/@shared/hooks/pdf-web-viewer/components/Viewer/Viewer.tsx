import { LegacyRef } from "react";
import { PdfWebViewerOptions } from "../../interfaces/pdf-web-viewer-options.interface";
import DialogProvider from "../../../dialog/DialogProvider";
import React from "react";

const Viewer = React.forwardRef((props: { config: PdfWebViewerOptions }, ref: LegacyRef<any>) => {
    return (
        <DialogProvider>
            <div className="web-viewer Button" style={{ position: 'fixed', top: 0, left: 0, right: 0, bottom: 0 }}>
                <div ref={ref} style={{ height: '100vh' }}></div>
            </div>
        </DialogProvider>
    )
});


export default Viewer;