
export const AnnotationCustomDataKeys = {
    isPublic: 'isPublic',
    sharedWith: 'sharedWith',
    userId: 'userId'
}