
export interface PdfWebViewerOptions {
    isReadOnly?: boolean;
    isPrint?: boolean;
    user?: { id: number, name: string };
    sharedWithUsers?: { id: number, name: string }[];
    licenseKey?: string;
    onSave?: (xfdfString: string) => Promise<null>;

    documentXFDFRetriever?: () => Promise<string>;

    download?: {
        isDownload: boolean,
        url: string,
        fileName: string
    };
    relativeDocuments?: {
        currentDocument: {
            name: string;
            documentId: number
        },
        onChange: (...params) => void,
        documents: {
            name: string;
            documentId: number
        }[]
    };
    onShare?: (instance) => void,
    cssPath?: string;
    libPath?: string;
    onClose?: () => void;
};
