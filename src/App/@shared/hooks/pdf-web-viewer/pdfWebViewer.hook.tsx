import { FormControl, makeStyles, MenuItem } from "@material-ui/core";
import Select from '@material-ui/core/Select';
import WebViewer, { WebViewerInstance, WebViewerOptions } from "@pdftron/webviewer";
import React, { LegacyRef, useRef, useState } from "react";
import ReactDOM, { unmountComponentAtNode } from "react-dom";
import DialogProvider from "../dialog/DialogProvider";
import { useDialog } from "../dialog/useDialog";
import AnnotationShare from "./components/Annotation/AnnotationShareOptions";
import { CloseButton } from "./components/Header/CloseButton";
import { SaveButton } from "./components/Header/SaveButton";
import { ShareButton } from "./components/Header/ShareButton";
import SwapDocument from "./components/SwapDocument/SwapDocument";
import Viewer from "./components/Viewer/Viewer";
import { AnnotationCustomDataKeys } from "./constants/constants";
import { PdfWebViewerOptions } from "./interfaces/pdf-web-viewer-options.interface";


const usePdfWebViewer = (): { open: (fileUrl: string, config?: PdfWebViewerOptions) => Promise<WebViewerInstance>, close: () => void, instance: WebViewerInstance } => {
    const viewerInstance = useRef<WebViewerInstance>(null);
    const viewerContainerEl = useRef(null);
    const swapDocumentContainerEl = useRef(null);
    const viewer = useRef<HTMLElement>(null);
    const isAnyAnnotationDeletedFromLastSave = useRef(false);
    const loadedAnnotationsIds = useRef([]);
    const { openMessage, openConfirm } = useDialog();
    const viewerConfig = useRef(null);


    const getViewerContainer = () => {
        const viewerContainer = document.createElement('div');
        viewerContainer.className = 'web-viewer-wrapper';
        viewerContainer.style.position = 'fixed';
        viewerContainer.style.zIndex = '99999';
        return viewerContainer;
    }

    const getSwapDocumentContainer = () => {
        const viewerContainer = document.createElement('div');
        viewerContainer.className = 'swap-document';
        viewerContainer.style.position = 'fixed';
        viewerContainer.style.zIndex = '99999';
        return viewerContainer;
    }

    const close = () => {
        !!viewerConfig.current.onClose && viewerConfig.current.onClose();
        unmountComponentAtNode(viewer.current);
        viewerContainerEl.current.remove();
        swapDocumentContainerEl.current.remove();
    }
   

    const isDirty = () => {
        return (isAnyAnnotationDeletedFromLastSave.current && ((loadedAnnotationsIds.current.length > viewerInstance.current.annotManager.getAnnotationsList().length) || viewerInstance.current.annotManager.getAnnotationsList().find(a => !loadedAnnotationsIds.current.includes(a.Id))))
            || !!viewerInstance.current.annotManager.getAnnotationsList().find(a => a.IsAdded || a.IsModified);
    }

    const open = (fileUrl: string, config: PdfWebViewerOptions = { isPrint: true, isReadOnly: false }): Promise<any> => {
        return new Promise((res, rej) => {
            function renderViewerContainer() {
                viewerContainerEl.current = getViewerContainer();
                document.body.appendChild(viewerContainerEl.current);
                ReactDOM.render(<Viewer ref={viewer} config={config} />, viewerContainerEl.current);
            }
            function openWebViewer() {
                const initialConfig: WebViewerOptions = {
                    ...config,
                    css: config.cssPath,
                    path: config.libPath || 'webviewer/lib',
                    initialDoc: fileUrl,
                    disabledElements: [
                        typeof (config.isPrint) === 'boolean' && !config.isPrint ? 'printButton' : null,
                        'noteState',
                        'thumbMultiDelete',
                        'thumbMultiRotate',
                        'thumbExtract',
                        'documentControl',
                        typeof (config.isReadOnly) === 'boolean' && config.isReadOnly ? 'toggleNotesButton' : null,
                    ],
                    extension: 'xod',
                    streaming: true,
                    documentXFDFRetriever : config.isReadOnly? null : config.documentXFDFRetriever
                };
                viewerConfig.current = initialConfig;
                WebViewer(initialConfig, viewer.current).then((instance) => {
                    viewerInstance.current = instance;
                    if (!!config.relativeDocuments) {
                        swapDocumentContainerEl.current = getSwapDocumentContainer();
                        document.body.appendChild(swapDocumentContainerEl.current);
                        ReactDOM.render(
                            <DialogProvider>
                                <SwapDocument relativeDocuments={config.relativeDocuments} isDirtyFunc={isDirty} />
                            </DialogProvider>, swapDocumentContainerEl.current);
                    }
                    function init() {
                        const { annotManager, docViewer } = instance;
                        if (!!config.user)
                            annotManager.setCurrentUser(config.user.name);
                        const serialize = instance.Annotations.Annotation.prototype.serialize;
                        instance.Annotations.Annotation.prototype.serialize = function (element, pageMatrix) {
                            const el: any = serialize.apply(this, [element, pageMatrix]);
                            el.children.forEach(c => {
                                if (c.tagName === 'trn-custom-data') {
                                    const customData = JSON.parse(c.getAttribute('bytes'));
                                    Object.keys(customData).forEach(k => {
                                        el.setAttribute(k, customData[k]);
                                    });
                                    c.remove();
                                }
                            });
                            return el;
                        };
                        const deserialize = instance.Annotations.Annotation.prototype.deserialize;
                        instance.Annotations.Annotation.prototype.deserialize = function (element, pageMatrix) {
                            const trnCustomDataEl = document.createElement('trn-custom-data');
                            const customData = JSON.stringify(
                                {
                                    [AnnotationCustomDataKeys.isPublic]: element.getAttribute(AnnotationCustomDataKeys.isPublic),
                                    [AnnotationCustomDataKeys.sharedWith]: element.getAttribute(AnnotationCustomDataKeys.sharedWith),
                                    [AnnotationCustomDataKeys.userId]: element.getAttribute(AnnotationCustomDataKeys.userId)
                                });
                            trnCustomDataEl.setAttribute('bytes', customData);
                            element.append(trnCustomDataEl);
                            deserialize.apply(this, [element, pageMatrix]);
                        };

                        docViewer.on('annotationsLoaded', () => {
                            if (!!instance.annotManager.getAnnotationsList().length)
                                instance.openElements(['notesPanel']);
                            loadedAnnotationsIds.current = annotManager.getAnnotationsList().map(a => a.Id);
                            annotManager.on('annotationChanged', (annotations, action) => {
                                if (action === 'add') {
                                    annotations.forEach(a => {
                                        a.setCustomData(AnnotationCustomDataKeys.isPublic, false);
                                        a.setCustomData(AnnotationCustomDataKeys.sharedWith, '');
                                        a.setCustomData(AnnotationCustomDataKeys.userId, !!config.user ? config.user.id : 0);
                                    });
                                }
                                if (action === 'delete') {
                                    isAnyAnnotationDeletedFromLastSave.current = true;
                                }
                            });
                        });


                    }
                    function customiseHeader() {

                        function save() {
                            instance.annotManager.exportAnnotCommand().then(xfdfString => {
                                !!config.onSave && config.onSave(xfdfString).then(() => {
                                    isAnyAnnotationDeletedFromLastSave.current = false;
                                    openMessage({ title: 'שמירת שינויים', message: 'שינויים נשמרו בהצלחה' });
                                }).catch(() => {
                                    openMessage({ title: 'שמירת שינויים', message: 'שגיאה - לא ניתן לשמור את השינויים' });
                                });
                            });
                        }
                        instance.setHeaderItems((header) => {
                            if (!config.isReadOnly && !!config.onSave) {
                                const onSave = () => {
                                    if (isDirty()) {
                                        save();
                                    }
                                    else {
                                        openMessage({ title: 'שמירת שינויים', message: 'לא התבצעו שינויים במסמך' });
                                    }

                                }
                                header.push(SaveButton(onSave));
                            }
                            const onClose = () => {
                                if (!config.isReadOnly && isDirty()) {
                                    openConfirm({
                                        title: 'שמירת שינויים', message: 'שינויים שלא נשמרו. לשמור אותם לפני יציאה?', okText: 'כן', cancelText: 'לא',
                                        onOk: () => { save(); close(); },
                                        onCancel: () => {
                                            close();
                                        },
                                    });
                                }
                                else {
                                    close();
                                }
                            }
                            header.push(CloseButton(onClose));


                            if (!!config.onShare) {
                                const onShare = () => {
                                    config.onShare(instance);
                                }
                                header.push(ShareButton(onShare));
                            }

                        });

                        if (!!config.download && config.download.isDownload) {
                            instance.settingsMenuOverlay.add([{
                                type: 'actionButton',
                                className: "row",
                                img: 'icon-header-download',
                                onClick: () => {
                                    function download(url, filename) {
                                        fetch(url).then(function (t) {
                                            return t.blob().then((b) => {
                                                var a = document.createElement("a");
                                                a.href = URL.createObjectURL(b);
                                                a.setAttribute("download", filename);
                                                a.click();
                                            }
                                            );
                                        });
                                    }
                                    download(config.download.url, config.download.fileName);
                                },
                                dataElement: 'alertButton',
                                label: 'Download'
                            }]);
                        }
                    }
                    function customizeNotesPanel() {
                        if (!config.isReadOnly) {
                            instance.dangerouslySetNoteTransformFunction((wrapper, state, createElement) => {
                                if (state.annotation.getCustomData(AnnotationCustomDataKeys.userId) == config.user.id) {
                                    const div = createElement('div');
                                    wrapper.prepend(div);

                                    ReactDOM.render(<AnnotationShare annotation={state.annotation} usersToShareWith={config.sharedWithUsers || []} />, div);
                                }
                            });
                            instance.annotManager.setPermissionCheckCallback((author, annotation) => {
                                return config.user.id == annotation.getCustomData(AnnotationCustomDataKeys.userId);
                            });
                        }
                    }
                    init();
                    customiseHeader();
                    customizeNotesPanel();

                    res(viewerInstance.current);
                });
            }
            renderViewerContainer();
            openWebViewer();
        });
    }

    return { open, close, instance: viewerInstance.current };
}

export default usePdfWebViewer;