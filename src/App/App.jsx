import React from 'react';
import Layout from './@core/layout/Layout';
import {
  Route,
  Switch
} from "react-router-dom";
import FastViewAttachments from './pages/fast-view-attachments/FastViewAttachments';
function App() {
  return (
    <div className="app">
      <Switch>
        <Route>
          <FastViewAttachments />
        </Route>
        {/* <Route path="/fast-view-attachments">
          <FastViewAttachments />
        </Route>
        <Route>
          <Layout />
        </Route> */}
      </Switch>
    </div>
  );
}

export default App;
