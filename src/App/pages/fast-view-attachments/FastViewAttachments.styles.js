import styled from "styled-components";


const FastViewAttachmentsStyled = styled.div`
.section{
    display: flex;
    flex-direction: column;
    height: 100vh;
}

.topic-attachments{
            overflow: auto;
    }

.committees,.meetings{
        .category-list{
            overflow: auto;
        }
    }


`;


export { FastViewAttachmentsStyled }

