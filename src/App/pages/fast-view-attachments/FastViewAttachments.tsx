import { WebViewerInstance } from '@pdftron/webviewer';
import queryString from 'query-string';
import React, { useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useLocation } from 'react-router';
import config from '../../../config';
import { get, post } from '../../@core/services/http.service';
import usePdfWebViewer from '../../@shared/hooks/pdf-web-viewer/pdfWebViewer.hook';
import CategoryList from './components/CategoryList/CategoryList';
import MeetingDocuments from './components/MeetingDocuments/MeetingDocument';
import TopicAttachments from './components/TopicAttachments/TopicAttachments';
import { FastViewAttachmentsStyled } from './FastViewAttachments.styles';
import { getAnnotations, getDocumentUserPermission, keepAlive, markAttachmentAsNew, saveAnnotations, share } from './services/FastViewAttachments.service';
import { getData } from './store/async-actions';
import { actions } from './store/slice';

const FastViewAttachments = () => {
    const { open, close, instance } = usePdfWebViewer();
    const { committees, meetings, topics, meetingsDocs, attachments, loaded, filteredMeetings, filteredTopics, selectedCommittee, selectedMeeting, selectedTopic, filteredAttachments, committeesSort, meetingsSort } = useSelector((state: any) => state.fastViewAttachments);
    const location = useLocation();
    const dispatch = useDispatch();
    const history = useHistory();
    const openedAttachmentIntervalId = useRef(null);
    useEffect(() => {
        dispatch(getData());
    }, [])


    const topicSelected = (topic) => {
        dispatch(actions.topicSelected(topic));
    }

    const committeeSelected = (committee) => {
        dispatch(actions.committeeSelected(committee));
    }
    const meetingSelected = (meeting) => {
        dispatch(actions.meetingSelected(meeting));
    }

    const initPageStateFromQuery = (query) => {
        if (!!Object.keys(query).length) {
            const committeeId = !!query['committeeId'] && parseInt(query['committeeId']);
            if (!!committeeId) {
                committeeSelected(committees.find(c => c.committeeId === committeeId));
                const meetingId = !!query['meetingId'] && parseInt(query['meetingId']);
                if (!!meetingId) {
                    meetingSelected(meetings.find(m => m.meetingId === meetingId));
                }
                const topicId = !!query['topicId'] && parseInt(query['topicId']);
                if (!!topicId) {
                    topicSelected(topics.find(t => t.meetingTopicId === topicId));
                }
            }
        }
        else {
            dispatch(actions.clear())
        }
    }

    useEffect(() => {
        if (loaded) {
            const query: any = queryString.parse(location.search);
            initPageStateFromQuery(query);
        }
    }, [loaded])

    useEffect(() => {
        const query: any = queryString.parse(location.search);
        initPageStateFromQuery(query);
    }, [location.search])


    const onCommitteeSelected = (committee) => {
        committeeSelected(committee);
        history.push(`?committeeId=${committee.committeeId}`);
    }

    const onMeetingSelected = (meeting) => {
        meetingSelected(meeting);
        let { committeeId, meetingId }: any = queryString.parse(location.search)
        meetingId = meeting.meetingId;
        history.push(`?${queryString.stringify({ committeeId, meetingId })}`);
    }

    const onTopicSelected = (topic) => {
        topicSelected(topic);
        const query: any = queryString.parse(location.search)
        query['topicId'] = topic.meetingTopicId;
        history.push(`?${queryString.stringify(query)}`);
    }

    const isAnyMeetingDocuments = () => {
        return !!meetingsDocs?.length && !!selectedMeeting && !!meetingsDocs.find(m => m.meetingId === selectedMeeting.meetingId);
    }

    const isAnyAttachments = () => {
        return !!filteredAttachments.length;
    }


    const onAttachmentClick = async (a) => {
        async function openViewer(attachment) {
            const data = await getDocumentUserPermission(attachment.meetingTopicDocumentId);
            open(`${config.api.url}${data.viewer.options.initialDoc}`, {
                isPrint: data.viewer.permissions.print,
                download: {
                    isDownload: data.viewer.permissions.download,
                    fileName: data.document.name,
                    url: data.document.downloadUrl
                },
                isReadOnly: data.viewer.options.enableReadOnlyMode,
                user: { id: data.user.id, name: data.viewer.options.annotationUser },
                licenseKey: data.viewer.options.l,
                sharedWithUsers: data.sharedUsers.map(u => { return { id: u.id, name: u.username } }),
                onSave: (xfdfString) => {
                    return new Promise((res, rej) => {
                        saveAnnotations(data.viewer.options.saveServerUrl, data.viewer.options.documentId, xfdfString).then(() => {
                            res(null);
                        }).catch(() => {
                            rej();
                        })
                    })
                },
                documentXFDFRetriever: () => {
                    return new Promise((resolve) => {
                        
                        getAnnotations(data.viewer.options.serverUrl, data.viewer.options.documentId).then(async (res) => {
                            const text = await res.data;
                            resolve(text);
                        });
                    });
                },
                relativeDocuments: {
                    currentDocument: { name: attachment.fullName, documentId: attachment.meetingTopicDocumentId },
                    documents: data.relatedDocs,
                    onChange: (documentId) => {
                        close();
                        const attachment = attachments.find(a => a.meetingTopicDocumentId === documentId);
                        openViewer(attachment);
                        dispatch(actions.viewerSwapDocument(attachment));
                    }
                },
                // onShare: (instance: WebViewerInstance) => {
                //     share(attachment.meetingTopicDocumentId, instance.docViewer.getCurrentPage());
                // },
                cssPath: config.viewer.cssPath,
                libPath: config.viewer.libPath,
                onClose: () => {
                    clearInterval(openedAttachmentIntervalId.current);
                }
            }).then(() => {
                openedAttachmentIntervalId.current = setInterval(() => {
                    keepAlive();
                }, 1500000);
            })
        }
        openViewer(a);
        dispatch(actions.attachmentClick(a));
    }

    const onAttachmentWatchedClick = (a) => {
        markAttachmentAsNew(a.meetingTopicDocumentId).then(() => {
            dispatch(actions.markAttachmentAsUnwatched(a));
        });
    }


    const onDocumentClick = async (d) => {
        const data = await getDocumentUserPermission(d.meetingDocumentId, 2);
        open(`${config.api.url}${data.viewer.options.initialDoc}`, {
            isPrint: data.viewer.permissions.print,
            download: {
                isDownload: data.viewer.permissions.download,
                fileName: data.document.name,
                url: data.document.downloadUrl
            },
            isReadOnly: true,
            user: { id: data.user.id, name: data.viewer.options.annotationUser },
            licenseKey: data.viewer.options.l
        });
    }

    const sortCommittees = () => {
        dispatch(actions.sortCommittees(committeesSort === 'asc' ? 'desc' : 'asc'));
    }

    const sortMeetings = () => {
        dispatch(actions.sortMeetings(meetingsSort === 'asc' ? 'desc' : 'asc'));
    }


    return (
        <FastViewAttachmentsStyled className="p-2 grid grid-cols-4 gap-2 h-screen overflow-hidden">
            <div className="committees section">
                <div className="flex items-center">
                    <h2 className="title ml-2">ועדות</h2>
                    <button onClick={() => sortCommittees()}><i className={`fa fa-arrow-circle-${committeesSort === 'asc' ? 'up' : 'down'} self-end text-2xl`}></i></button>
                </div>
                <CategoryList items={committees} onItemSelectedEvt={onCommitteeSelected} selected={selectedCommittee} idKey="committeeId" />
                <a href="/" className="bg-black px-6 py-4 rounded text-white self-start mt-auto mb-4"><i className="fas fa-home"></i></a>

            </div>
            <div className="meetings section">
                <div className="flex items-center">
                    <h2 className="title ml-2">ישיבות</h2>
                    <button onClick={() => sortMeetings()}><i className={`fa fa-arrow-circle-${meetingsSort === 'asc' ? 'up' : 'down'} self-end text-2xl`}></i></button>
                </div>
                <CategoryList items={filteredMeetings} onItemSelectedEvt={onMeetingSelected} selected={selectedMeeting} idKey="meetingId" transformFunc={(item) => item.dateTimeString} color={selectedCommittee?.color} />
            </div>
            <div className="section">
                <h2 className="title">&nbsp;</h2>
                <div className="overflow-auto">
                    {
                        isAnyMeetingDocuments() ? (
                            <>
                                <h3 className="border-2 border-gray-400 font-bold p-2 mb-1 text-lg bg-gray-100">מסמכי הישיבה:</h3>
                                <MeetingDocuments documents={meetingsDocs.filter(m => m.meetingId === selectedMeeting.meetingId)} onDocumentClick={onDocumentClick} />
                                <h3 className="border-2 border-gray-400 font-bold p-2 mb-1 text-lg bg-gray-100">נושאי הישיבה:</h3>
                                <CategoryList items={filteredTopics} onItemSelectedEvt={onTopicSelected} selected={selectedTopic} idKey="meetingTopicId" transformFunc={(item) => `${item.topicOrder}. ${item.topicName}`} color={selectedCommittee?.color} />
                            </>
                        ) : <CategoryList items={filteredTopics} onItemSelectedEvt={onTopicSelected} selected={selectedTopic} idKey="meetingTopicId" transformFunc={(item) => `${item.topicOrder}. ${item.topicName}`} color={selectedCommittee?.color} />
                    }
                </div>

            </div>
            {
                isAnyAttachments() ?
                    <div className="section">
                        <h2 className="title">חומרים נלווים</h2>
                        <TopicAttachments attachments={filteredAttachments} onAttachmentClick={onAttachmentClick} onAttachmentWatchedClick={onAttachmentWatchedClick} />
                    </div> : null
            }

        </FastViewAttachmentsStyled>
    )
}


export default FastViewAttachments;



