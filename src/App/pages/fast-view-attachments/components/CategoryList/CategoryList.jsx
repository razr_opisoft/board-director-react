import React, { useEffect, useState } from "react";
import CategoryListItem from "./CategoryListItem/CategoryListItem";
import { CategoryListItemStyled, CategoryListStyled, ListItemStyled } from "./CategoryList.styles";
const CategoryList = (props) => {
    const [items, setItems] = useState(props.items);
    const [selected, setSelected] = useState(props.selected);
    const onItemSelected = (item) => {
        if (selected !== item) {
            setSelected(item);
            !!props.onItemSelectedEvt && !!props.onItemSelectedEvt(item);
        }
    }

    useEffect(() => {
        setItems(props.items);
    }, [props.items]);

    useEffect(() => {
        setSelected(props.selected);
    }, [props.selected]);

    const isSelected = (item) => {
        return !!selected && (!!props.idKey ? selected[props.idKey] === item[props.idKey] : selected.id === item.id );
    }

    return (<ul className="category-list">
        {
            items.map(item => {
                return (
                    <CategoryListItemStyled key={!!props.idKey ? item[props.idKey] : item.id} className={`cursor-pointer ${isSelected(item) ? 'active' : ''}`} onClick={() => onItemSelected(item)} categoryColor={item.color || props.color}>
                        <CategoryListItem unread={item.unread} name={!!props.transformFunc ? props.transformFunc(item) : item.name} isHighlight={isSelected(item)} iconClass={item.iconClass}/>
                    </CategoryListItemStyled>
                )
            })
        }
    </ul>);
}

export default CategoryList;

