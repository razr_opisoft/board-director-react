import styled from "styled-components";

const ListItemStyled = styled.li`
display : flex;
align-items: center;
padding: 0.5rem;
border-width: 1px;
border-radius: 0.375rem;
border-color: rgb(107, 114, 128);
background-color: rgb(243, 244, 246);
margin-bottom : .5rem;
`

const CategoryListItemStyled = styled(ListItemStyled)`
    border-right : 10px solid ${props => props.categoryColor};
    &.active{
    border-right-width:20px;
    }
`
export { ListItemStyled, CategoryListItemStyled};