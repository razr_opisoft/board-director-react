import React from 'react';

const CategoryListItem = ({ iconClass = '', name = '', unread = 0 }) => {
    return (
        <>
            {
                !!iconClass ? <i className={`ml-2 ${iconClass}`}></i> : null
            }
            <span className="ml-4">{name}</span>
            {
                !!unread ? <span className="mr-auto py-1 px-1.5 text-white bg-black rounded-md">{unread}</span> : null
            }
        </>
    )
}

export default CategoryListItem;
