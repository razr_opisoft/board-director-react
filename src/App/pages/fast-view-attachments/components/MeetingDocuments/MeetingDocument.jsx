import { ListItemStyled } from "../CategoryList/CategoryList.styles";
import CategoryListItem from "../CategoryList/CategoryListItem/CategoryListItem";

const MeetingDocuments = ({ documents , onDocumentClick }) => {
    return (
         <ul className="meeting-attachments">
            {
                documents.map(d => {
                    return (
                        <ListItemStyled key={d.meetingDocumentId} className="cursor-pointer" onClick={() => !!onDocumentClick && onDocumentClick(d)}>
                            <i className="ml-2 fa fa-file"></i>
                            <span>{d.fileName}</span>
                        </ListItemStyled>
                    )
                })
            }
        </ul>
    )
}

export default MeetingDocuments;