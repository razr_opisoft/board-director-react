import { ListItemStyled } from "../CategoryList/CategoryList.styles";

const TopicAttachments = ({ attachments = [], onAttachmentClick, onAttachmentWatchedClick }) => {
    const onItemClicked = (a) => {
        !!onAttachmentClick && onAttachmentClick(a);
    }
    const onWatchedClickd = (a) => {
        !!onAttachmentWatchedClick && onAttachmentWatchedClick(a);
    }
    return (
        <ul className="topic-attachments">
            {
                attachments.map(a => {
                    return (
                        <ListItemStyled key={a.meetingTopicDocumentId} className="cursor-pointer" onClick={() => onItemClicked(a)}>
                            <span className="ml-2">{a.attachmentOrder}. {`(${a.pageCount})`} {a.name}</span>
                            { a.isDocumentViewd ? <i className="ml-2 fa fa-eye" onClick={(e) => { e.stopPropagation(); onWatchedClickd(a) }}></i> : null}
                        </ListItemStyled>
                    )
                })
            }
        </ul>
    )
}


export default TopicAttachments;