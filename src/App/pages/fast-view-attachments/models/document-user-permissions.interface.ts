export interface User {
    username: string;
    isAdmin: boolean;
    isReadOnly: boolean;
    id: number;
}

export interface Document {
    name: string;
    extention: string;
    pages: number;
    downloadUrl: string;
}

export interface Options {
    type: string;
    documentType: string;
    initialDoc: string;
    enableAnnotations: boolean;
    serverUrl: string;
    documentId: string;
    streaming: boolean;
    config: string;
    path: string;
    custom: string;
    l: string;
    annotationUser: string;
    annotationAdmin: boolean;
    enableReadOnlyMode: boolean;
    enablePublicAnnotations: boolean;
    saveServerUrl : string;
}

export interface Permissions {
    download: boolean;
    print: boolean;
    selectText: boolean;
    redact: boolean;
}

export interface Error {
    title?: any;
    content?: any;
}

export interface Viewer {
    options: Options;
    permissions: Permissions;
    error: Error;
    rtl: boolean;
}

export interface SharedUser {
    username: string;
    isAdmin: boolean;
    isReadOnly: boolean;
    id: number;
}

export interface DocumentUser {
    username: string;
    isAdmin: boolean;
    isReadOnly: boolean;
    id: number;
}

export interface RelatedDoc {
    name: string;
    documentId: number;
    viewerType: string;
    storageType: number;
}

export interface GetDocumentUserPermissions {
    user: User;
    document: Document;
    viewer: Viewer;
    allowedViewersSummary: string;
    sharedUsers: SharedUser[];
    documentUsers: DocumentUser[];
    relatedDocs: RelatedDoc[];
}
