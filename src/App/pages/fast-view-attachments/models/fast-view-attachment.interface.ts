export interface Committee {
    unread: number;
    committeeId: number;
    name: string;
    color: string;
    distributionType: number;
}


export interface MeetingDoc{
    fileName: string;
    meetingDocumentId: number;
    meetingDocumentType: number;
    meetingId: number;
}


export interface Meeting {
    date: string;
    dateTimeString: string;
    meetingId: number;
    committeeId: number;
    unread: number;
}



export interface Topic {
    meetingTopicId: number;
    meetingId: number;
    topicName: string;
    startTime: string;
    endTime: string;
    topicOrder: number;
    unread: number;
}

export interface Attachment {
    name: string;
    fullName: string;
    meetingTopicDocumentId: number;
    isDocumentViewd: boolean;
    isDocumentAnnotated: boolean;
    attachmentOrder: number;
    isConverted: boolean;
    meetingTopicId: number;
    pageCount: number;
}


export interface GetFastViewAttachments {
    committees: Committee[];
    meetings: Meeting[];
    topics: Topic[];
    attachments: Attachment[];
}
