import { GetDocumentUserPermissions } from '../models/document-user-permissions.interface';
import config from '../../../../config';
import { get, post } from '../../../@core/services/http.service';
export async function getFastViewAttachmentsData() {
    const res = await get(`${config.api.url}/WebViewerV2/GetFastViewAttachmentsData`)
    return res.data;
}


export async function getDocumentUserPermission(documentId: number, storageType = 3, viewerType = 'comment'): Promise<GetDocumentUserPermissions> {
    const res = await get(`${config.api.url}/WebViewerV2/GetDocumentUserPermissions`, { params: { documentId, storageType, viewerType } });
    return res.data;
}

export async function markAttachmentAsNew(documentId) {
    const res = await post(`${config.api.url}/WebViewerV2/MarkAsNew`, null, { params: { meetingTopicDocumentId: documentId } });
    return res.data;
}

export async function getMeetingsDocs(meetingIds) {
    const res = await post(`${config.api.url}/WebViewerV2/GetMeetingsDocs`, { meetingIds });
    return res.data;
}

export async function share(documentId, pageNumber) {
    const res = await post(`${config.api.url}/WebViewerV2/Share`, null, { params: { meetingTopicDocumentId: documentId, pageNumber } });
    return res.data;
}


export async function keepAlive() {
    const res = await post(`${config.api.url}/Home/KeepSessionAlive`);
    return res.data;
}


export async function getAnnotations(serverUrl, documentId) {
    return get(`${config.api.url}${serverUrl}?did=${documentId}`);
}


export async function saveAnnotations(saveServerUrl, documentId, xfdfString) {
    return post(`${config.api.url}${saveServerUrl}?did=${documentId}`, xfdfString)
}