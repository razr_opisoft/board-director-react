import { createAsyncThunk } from '@reduxjs/toolkit';
import { getFastViewAttachmentsData, getMeetingsDocs } from "../services/FastViewAttachments.service";
import { actions, slice } from "./slice";

const getData = createAsyncThunk(
    `${slice}/getData`,
    async (args, thunkAPI) => {
        try {
            const fastViewAttachmentsData = await getFastViewAttachmentsData();
            fastViewAttachmentsData.committees = fastViewAttachmentsData.committees.map(c => { return { ...c, iconClass: 'fa fa-users' } });
            const meetingsDocs = await getMeetingsDocs(fastViewAttachmentsData.meetings.map(m => m.meetingId));
            thunkAPI.dispatch(actions.init({ ...fastViewAttachmentsData, meetingsDocs: meetingsDocs })); // SUCCESS
        } catch (error) {
            meetingsDocs
            // FAILED 
        }
    }
)

export { getData };

