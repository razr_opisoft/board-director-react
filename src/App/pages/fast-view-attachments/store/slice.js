import { createSlice } from '@reduxjs/toolkit';
export const slice = 'fastViewAttachments';

const sortList = (a, b, mode) => {
    if (mode === 'asc') {
        return a > b ? 1 : -1;
    }
    else {
        return a < b ? 1 : -1;
    }
}
const fastViewAttachmentsSlice = createSlice({
    name: slice,
    initialState: {
        loaded: false,
        committees: [],
        meetings: [],
        topics: [],
        attachments: [],
        meetingAttachments: [],
        filteredMeetings: [],
        filteredTopics: [],
        filteredAttachments: [],
        selectedCommittee: null,
        selectedMeeting: null,
        selectedTopic: null,
        committeesSort: 'asc',
        meetingsSort: 'asc'
    },
    reducers: {
        init(state, action) {
            state.committees = action.payload.committees;
            state.meetings = action.payload.meetings;
            state.topics = action.payload.topics;
            state.attachments = action.payload.attachments;
            state.meetingsDocs = action.payload.meetingsDocs;
            state.loaded = true;
        },
        setCommittees(state, action) {
            state.committees = action.payload;
        },
        setMeetings(state, action) {
            state.meetings = action.payload;
        },
        setTopics(state, action) {
            state.topics = action.payload;
        },
        filterMeetings(state, action) {
            state.filteredMeetings = action.payload;
        },
        filterTopics(state, action) {
            state.filteredTopics = action.payload;
        },
        committeeSelected(state, action) {
            state.selectedCommittee = action.payload;
            state.selectedMeeting = null;
            state.selectedTopic = null;
            state.filteredMeetings = state.meetings.filter(m => m.committeeId === action.payload.committeeId);
            state.filteredTopics = [];
            state.filteredAttachments = [];
        },
        meetingSelected(state, action) {
            state.selectedMeeting = action.payload;
            state.selectedTopic = null;
            state.filteredTopics = state.topics.filter(t => t.meetingId === action.payload.meetingId).sort((a, b) => a.topicOrder > b.topicOrder ? 1 : -1)
            state.filteredAttachments = [];
        },
        topicSelected(state, action) {
            state.selectedTopic = action.payload;
            state.filteredAttachments = state.attachments.filter(a => a.meetingTopicId === action.payload.meetingTopicId).sort((a, b) => a.attachmentOrder > b.attachmentOrder ? 1 : -1);
        },
        attachmentClick(state, action) {
            if (!action.payload.isDocumentViewd) {
                state.committees.find(c => c.committeeId === state.selectedCommittee.committeeId).unread--;
                state.meetings.find(m => m.meetingId === state.selectedMeeting.meetingId).unread--;
                state.topics.find(t => t.meetingTopicId === state.selectedTopic.meetingTopicId).unread--;
                state.filteredMeetings.find(m => m.meetingTopicId === state.selectedMeeting.meetingTopicId).unread--;
                state.filteredTopics.find(t => t.meetingTopicId === state.selectedTopic.meetingTopicId).unread--;
                state.attachments.find(a => a.meetingTopicDocumentId === action.payload.meetingTopicDocumentId).isDocumentViewd = true;
                state.filteredAttachments.find(a => a.meetingTopicDocumentId === action.payload.meetingTopicDocumentId).isDocumentViewd = true;
            }
        },
        markAttachmentAsUnwatched(state, action) {
            state.committees.find(c => c.committeeId === state.selectedCommittee.committeeId).unread++;
            state.meetings.find(m => m.meetingId === state.selectedMeeting.meetingId).unread++;
            state.topics.find(t => t.meetingTopicId === state.selectedTopic.meetingTopicId).unread++;
            state.filteredMeetings.find(m => m.meetingId === state.selectedMeeting.meetingId).unread++;
            state.filteredTopics.find(t => t.meetingTopicId === state.selectedTopic.meetingTopicId).unread++;
            state.attachments.find(a => a.meetingTopicDocumentId === action.payload.meetingTopicDocumentId).isDocumentViewd = false;
            state.filteredAttachments.find(a => a.meetingTopicDocumentId === action.payload.meetingTopicDocumentId).isDocumentViewd = false;
        },
        sortCommittees(state, action) {
            state.committeesSort = action.payload;
            state.committees.sort((a, b) => sortList(a, b, state.committeesSort));
        },
        sortMeetings(state, action) {
            state.meetingsSort = action.payload;
            state.meetings.sort((a, b) => sortList(a, b, state.meetingsSort));
        },
        viewerSwapDocument(state, action) {
            if (!action.payload.isDocumentViewd) {
                const topic = state.topics.find(t => t.meetingTopicId === action.payload.meetingTopicId);
                topic.unread--;
                const filteredTopic = state.filteredTopics.find(t => t.meetingTopicId === action.payload.meetingTopicId);
                filteredTopic.unread--;
                state.attachments.find(a => a.meetingTopicDocumentId === action.payload.meetingTopicDocumentId).isDocumentViewd = true;
                const meeting = state.meetings.find(m => m.meetingId === topic.meetingId);
                meeting.unread--;
                state.filteredMeetings.find(m => m.meetingId === topic.meetingId).unread--;
                state.committees.find(c => c.committeeId === meeting.committeeId).unread--;
            }
        },
        clear(state) {
            state.filteredMeetings = [], state.filteredTopics = [], state.filteredAttachments = [];
            state.selectedCommittee = null, state.selectedMeeting = null, state.selectedTopic = null;
        }
    },
})

export const actions = fastViewAttachmentsSlice.actions;

export default fastViewAttachmentsSlice.reducer
