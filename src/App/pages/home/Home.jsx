import Banner from "./components/Banner/Banner";
import MyCommittees from "./components/MyCommittees/MyCommittees";
import MyProtocols from "./components/MyProtocols/MyProtocols";
import MyTasks from "./components/MyTasks/MyTasks";
import HomeStyled from "./Home.styles";

const Home = () => {
    return (
        <HomeStyled className="home">
            <Banner />
            <MyCommittees/>
            <MyProtocols/>
            <MyTasks/>
        </HomeStyled>
    )
};


export default Home;