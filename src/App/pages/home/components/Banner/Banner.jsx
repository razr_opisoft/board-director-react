import BannerStyled from "./Banner.styles";

const Banner = () =>{
    return (
        <BannerStyled className="banner bg-white rounded-xl px-7 py-10">
            <h1 className="title font-bold">צהריים טובים, <span className="user-name">תמיר גורן</span> </h1>
            <h2 className="subtitle">המשך יום נעים בעבודה</h2>
        </BannerStyled>
    )
}

export default Banner;