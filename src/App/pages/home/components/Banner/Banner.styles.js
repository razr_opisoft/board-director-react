import styled from 'styled-components';

const BannerStyled = styled.div`

        .title{
                font-size : 3rem;
        }
        .subtitle{
                font-size : 1.5rem;
            color:var(--gray-color);
        }

        .user-name{
        color:  var(--primary-color);
    }
`;

export default BannerStyled;