import CommitteeCardStyled from "./CommitteeCard.styles";

const CommitteeCard = () => {
    return (
        <CommitteeCardStyled className="committee-card inline-flex flex-col py-4 px-8 text-center bg-white rounded-2xl items-center">
            <i className="fas fa-user-friends p-3 text-2xl text-red-500 bg-red-100 rounded-xl"></i>
            <span className="font-medium text-lg leading-5 mt-2">ועדה לבחינת דוחות כספיים</span>
            <span className="text-3xl mt-1">4</span>
            <span className="subtitle">ישיבות עתידיות</span>
        </CommitteeCardStyled>
    )
}

export default CommitteeCard;