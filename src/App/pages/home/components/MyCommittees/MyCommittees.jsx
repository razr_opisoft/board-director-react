import CommitteeCard from "./CommitteeCard/CommitteeCard"
import MyCommitteesStyled from "./MyCommittees.styles";

const MyCommittees = () => {
    return (
        <MyCommitteesStyled className="my-committees">
            <h2 className="title mb-4">הוועדות שלי</h2>
            <div className="flex">
                <div className="cards">
                    <CommitteeCard />
                    <CommitteeCard />
                    <CommitteeCard />
                    <CommitteeCard />
                </div>
                <button className="show-more self-center text-3xl m-auto">
                    <i className="fas fa-chevron-left"></i>
                </button>
            </div>
        </MyCommitteesStyled>
    )
}


export default MyCommittees;