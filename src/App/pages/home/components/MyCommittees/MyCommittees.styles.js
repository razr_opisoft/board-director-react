import styled from 'styled-components';

const MyCommitteesStyled = styled.div`
.cards{
    width: 90%;
display : inline-grid;
gap : 2rem;
grid-template-columns : repeat(4, 1fr);
    .committee-card{
        flex: 0 0 20%;
    }

}

.show-more{
    color : var(--gray-color);
}
`


export default MyCommitteesStyled;