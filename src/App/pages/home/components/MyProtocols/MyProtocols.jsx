import React from 'react';
import MyProtocolsTable from './components/Table/MyProtocolsTable';


const MyProtocols = () => {
    return (
        <div className="my-protocols">
            <h2 className="title">פרוטוקולים להתייחסותי</h2>
            <MyProtocolsTable />
        </div>
    );
}

export default MyProtocols;