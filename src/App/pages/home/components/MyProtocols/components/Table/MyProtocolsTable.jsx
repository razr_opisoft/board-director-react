import { TableSortLabel } from '@material-ui/core';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import React from 'react';



const MyProtocolsTable = () => {
    return (
        <div className="bd-basic-table">
            <TableContainer>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell align="right">
                                 <TableSortLabel active direction="desc">
                                    שם הוועדה
                                </TableSortLabel>
                                </TableCell>
                            <TableCell align="right">
                                <TableSortLabel active direction="desc">
                                    תאריך הישיבה            
                            </TableSortLabel>
                            </TableCell>
                            <TableCell align="right">שם הנושא/הישיבה</TableCell>
                            <TableCell align="right">תאריך יעד</TableCell>
                            <TableCell align="center">סטטוס</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        <TableRow>
                            <TableCell align="right" className="relative">
                                <span style={{ backgroundColor:'#EFEFFF' , padding:'11px 16px' ,borderRadius  : '16px'}}>
                                    ועדת השקעות 
                                </span>
                                <div className="absolute" style={{ right: 0, height: '50%', width: '3px', backgroundColor:'#605BFC', top:'50%' , transform : 'translateY(-50%)',borderRadius: '3px'}}></div>
                                </TableCell>
                            <TableCell align="right">05/07/2021</TableCell>
                            <TableCell align="right">ועדת השקעות </TableCell>
                            <TableCell align="right">05/08/2021</TableCell>
                            <TableCell align="right"><img src="/icons/check_circle-24px.svg" alt="status-icon" className="m-auto" /></TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell align="right">
                                <span style={{ backgroundColor: '#FFF4E8', padding: '11px 16px', borderRadius: '22px' }}>
                                    ועדת אשראי              
                            </span>
                            <div className="absolute" style={{ right: 0, height: '50%', width: '3px', backgroundColor: '#FD8D25', top: '50%', transform: 'translateY(-50%)', borderRadius: '3px' }}></div>
                            </TableCell>
                            <TableCell align="right">05/07/2021</TableCell>
                            <TableCell align="right">ועדת השקעות </TableCell>
                            <TableCell align="right">05/08/2021</TableCell>
                            <TableCell align="right"><img src="/icons/watch_later-24px.svg" alt="status-icon" className="m-auto" /></TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell align="right">
                                <span style={{ backgroundColor: '#E9F9F7', padding: '11px 16px', borderRadius: '22px' }}>
                                    ועדת כספים
                                </span>
                            <div className="absolute" style={{ right: 0, height: '50%', width: '3px', backgroundColor: '#34CFB7', top: '50%', transform: 'translateY(-50%)', borderRadius: '3px' }}></div>
                            </TableCell>
                            <TableCell align="right">05/07/2021</TableCell>
                            <TableCell align="right">ועדת השקעות </TableCell>
                            <TableCell align="right">05/08/2021</TableCell>
                            <TableCell align="right"><img src="/icons/check_circle-24px.svg" alt="status-icon" className="m-auto" /></TableCell></TableRow>
                        <TableRow>
                            <TableCell align="right">
                                <span style={{ backgroundColor: '#FFF0F0', padding: '11px 16px', borderRadius: '22px' }}>
                                    ועדת ביקורת
                                </span>
                            <div className="absolute" style={{ right: 0, height: '50%', width: '3px', backgroundColor: '#FD6764', top: '50%', transform: 'translateY(-50%)', borderRadius: '3px' }}></div>
                            </TableCell>
                            <TableCell align="right">05/07/2021</TableCell>
                            <TableCell align="right">ועדת השקעות </TableCell>
                            <TableCell align="right">05/08/2021</TableCell>
                            <TableCell align="right"><img src="/icons/check_circle-24px.svg" alt="status-icon" className="m-auto" /></TableCell></TableRow>
                    </TableBody>
                </Table>
            </TableContainer>

            <div className="flex flex-row-reverse mt-2 ml-7">
                <button>
                    להרחבה
                <i className="fas fa-expand-arrows-alt mr-2"></i>
                </button>
            </div>
            
        </div>
    );
}

export default MyProtocolsTable;