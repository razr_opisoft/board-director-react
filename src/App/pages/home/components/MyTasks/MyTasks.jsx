import MyTasksTable from "./components/MyTasksTable"
import MyTasksStyled from "./MyTasks.styles";

const MyTasks = ()=>{
    return (
        <MyTasksStyled className="my-tasks">
            <h2 className="title">
                <span className="commeettee-tasks ml-2">משימות מוועדה</span> משימות כלליות
            </h2>
            <MyTasksTable/>
        </MyTasksStyled>
    )
};

export default MyTasks;