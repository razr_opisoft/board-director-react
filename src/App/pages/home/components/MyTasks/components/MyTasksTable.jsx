import { TableSortLabel } from '@material-ui/core';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import React from 'react';



const MyTasksTable = () => {
    return (
        <div className="bd-basic-table">
            <TableContainer>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell align="right">
                                <span className="align-middle">משימה</span>
                                <button className="mr-2 align-middle">
                                    <i className="fas fa-plus-circle"></i>
                                </button>
                                </TableCell>
                            <TableCell align="right">
                                יוצר המשימה
                            </TableCell>
                            <TableCell align="right">תאריך יצירה</TableCell>
                            <TableCell align="right">תאריך יעד</TableCell>
                            <TableCell align="center">קבצים</TableCell>
                            <TableCell align="center">סטטוס</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody >
                        <TableRow>
                            <TableCell align="right">
                                יש להתעדכן בחוק החברות החדש ולוודא שכל החוקים במערכת במנוע החוקים תואמים
                                </TableCell>
                            <TableCell align="right">Board Admin SQLink</TableCell>
                            <TableCell align="right">05/07/2021</TableCell>
                            <TableCell align="right">05/08/2021</TableCell>
                            <TableCell align="center">
                                <i className="fas fa-file-download"></i>
                            </TableCell>
                            <TableCell align="right"><img src="/icons/check_circle-24px.svg" alt="status-icon" className="m-auto" /></TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell align="right">להעלות קבצים חסרים לישיבת כספים הבאה
                            </TableCell>
                            <TableCell align="right">Board Admin SQLink</TableCell>
                            <TableCell align="right">05/07/2021</TableCell>
                            <TableCell align="right">05/08/2021</TableCell>
                            <TableCell align="center">
                            </TableCell>
                            <TableCell align="right"><img src="/icons/watch_later-24px.svg" alt="status-icon" className="m-auto" /></TableCell>
                        </TableRow>
                        <TableRow>
                            <TableCell align="right">לפתוח משתמש לדירקטור חדש בוועדת הנהלה
                            </TableCell>
                            <TableCell align="right">Board Admin SQLink</TableCell>
                            <TableCell align="right">05/07/2021</TableCell>
                            <TableCell align="right">05/08/2021</TableCell>
                            <TableCell align="center">
                                <i className="fas fa-file-download"></i>
                            </TableCell>
                            <TableCell align="right"><img src="/icons/check_circle-24px.svg" alt="status-icon" className="m-auto" /></TableCell></TableRow>
                        <TableRow>
                            <TableCell align="right">יש להוציא דו"ח לבנק ישראל עבור 2020
                            </TableCell>
                            <TableCell align="right">Board Admin SQLink</TableCell>
                            <TableCell align="right">05/07/2021</TableCell>
                            <TableCell align="right">05/08/2021</TableCell>
                            <TableCell align="center">
                                <i className="fas fa-file-download"></i>
                            </TableCell>
                            <TableCell align="right"><img src="/icons/check_circle-24px.svg" alt="status-icon" className="m-auto" /></TableCell></TableRow>
                    </TableBody>
                </Table>
            </TableContainer>

            <div className="flex flex-row-reverse mt-2 ml-7">
                <button>
                    להרחבה
                <i className="fas fa-expand-arrows-alt mr-2"></i>
                </button>
            </div>
            
        </div>
    );
}

export default MyTasksTable;