import { combineReducers } from "redux";
import fastViewAttachmentsReducer from "../pages/fast-view-attachments/store/slice";

const rootReducer = combineReducers({
    fastViewAttachments: fastViewAttachmentsReducer,
})

export default rootReducer;