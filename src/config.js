const dev = {
    api: {
        url: "https://www.board-director.com",
    },
    viewer: {
        cssPath: '',
        libPath: ''
    }
};

const prod = {
    api: {
        url: ""
    },
    viewer: {
        cssPath: '/react-app/webviewer/css/styles.css',
        libPath: '/react-app/webviewer/lib'
    }
};

const config = process.env.REACT_APP_STAGE === 'production'
    ? prod
    : dev;

export default {
    ...config
};