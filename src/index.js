import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { HashRouter as Router } from 'react-router-dom';
import DialogProvider from './app/@shared/hooks/dialog/DialogProvider';
import App from './app/App';
import store from './app/store/store';
import './index.scss';


ReactDOM.render(
  <Provider store={store}>
    <DialogProvider>
      <Router>
        <React.StrictMode>
          <App />
        </React.StrictMode>
      </Router >
    </DialogProvider>
  </Provider>
  , document.getElementById('root')
);